//
//  main.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 04/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//
#import <stdio.h>
#import <Foundation/Foundation.h>
#import "SPIStarSystem.h"
#import "SPISpaceObject.h"
#import "SPIStar.h"
#import "SPIPlanet.h"
#import "SPIAsteroidField.h"
#import "SPIPlayerSpaceship.h"


int main(int argc, const char * argv[]) {
    
//******************************************FIRST SYSTEM******************************************//
    
    SPIStarSystem *milkyStarSystem = [[SPIStarSystem alloc] initWithName:@"Milky Way" age:@(130000000)];
    
    SPIPlanet *earthPlanet = [[SPIPlanet alloc] initWithName:@"Earth"];
    earthPlanet.atmosphere = YES;
    earthPlanet.peoplesCount = 7000000000;
    //earthPlanet.destructible = NO;
    
    SPIPlanet *yupiterPlanet = [[SPIPlanet alloc] initWithName:@"Yupiter"];
    yupiterPlanet.atmosphere = NO;
    yupiterPlanet.peoplesCount = 0;
    //yupiterPlanet.destructible = NO;
    
    SPIPlanet *saturnPlanet = [[SPIPlanet alloc] initWithName:@"Saturn"];
    saturnPlanet.atmosphere = NO;
    saturnPlanet.peoplesCount = 0;
    //saturnPlanet.destructible = NO;
    
    SPIPlanet *moonPlanet = [[SPIPlanet alloc] initWithName:@"Moon"];
    moonPlanet.atmosphere = NO;
    moonPlanet.peoplesCount = 100;
    //moonPlanet.destructible = NO;
    
    SPIAsteroidField *milkyAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Milky Filed"];
    milkyAsteroidField.density = 70000000;
    //milkyAsteroidField.destructible = YES;
    
    SPIStar *sunStar = [[SPIStar alloc] initWithName:@"Sun"];
    sunStar.weight = 10000000;
    sunStar.type = SPIStarTypeMedium;
    //sunStar.destructible = NO;
    
    milkyStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[earthPlanet, yupiterPlanet, saturnPlanet, moonPlanet, milkyAsteroidField, sunStar]];
    
    //******************************************SECOND SYSTEM******************************************//
    
    SPIStarSystem *alphaStarSystem = [[SPIStarSystem alloc] initWithName:@"Sirius" age:@(230000000)];
    SPIPlanet *vulkanPlanet = [[SPIPlanet alloc] initWithName:@"Vulcan"];
    vulkanPlanet.atmosphere = YES;
    vulkanPlanet.peoplesCount = 325000000;
    //vulkanPlanet.destructible = NO;
    
    SPIAsteroidField *hotaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Hota"];
    hotaAsteroidField.density = 6000000;
    //hotaAsteroidField.destructible = YES;
    
    SPIPlanet *gallifreiPlanet = [[SPIPlanet alloc] initWithName:@"Gallifrey"];
    gallifreiPlanet.atmosphere = YES;
    gallifreiPlanet.peoplesCount = 700000000;
    //gallifreiPlanet.destructible = NO;
    
    SPIPlanet *nabooPlanet = [[SPIPlanet alloc] initWithName:@"Naboo"];
    nabooPlanet.atmosphere = YES;
    nabooPlanet.peoplesCount = 625000000;
    //nabooPlanet.destructible = NO;
    
    SPIPlanet *plutoPlanet = [[SPIPlanet alloc] initWithName:@"Pluto"];
    plutoPlanet.atmosphere = NO;
    //plutoPlanet.destructible = NO;
    
    SPIStar *zenStar = [[SPIStar alloc] initWithName:@"Zen"];
    zenStar.weight = 1000000000;
    zenStar.type = SPIStarTypeGiant;
    //zenStar.destructible = NO;
    
    alphaStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[vulkanPlanet, hotaAsteroidField, gallifreiPlanet, nabooPlanet, plutoPlanet, zenStar]];
    
    //******************************************THIRD SYSTEM******************************************//
    
    SPIStarSystem *realStarSystem = [[SPIStarSystem alloc] initWithName:@"REAL MADRID" age:@(200)];
    
    SPIPlanet *balePlanet = [[SPIPlanet alloc] initWithName:@"Gareth Bale"];
    balePlanet.atmosphere = YES;
    balePlanet.peoplesCount = 605000000;
    //balePlanet.destructible = NO;
    
    SPIAsteroidField *navasAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Keylor Navas"];
    navasAsteroidField.density = 10000000;
    //navasAsteroidField.destructible = YES;
    
    SPIPlanet *ronaldoPlanet = [[SPIPlanet alloc] initWithName:@"Cristiano Ronaldo"];
    ronaldoPlanet.atmosphere = YES;
    ronaldoPlanet.peoplesCount = 900000000;
    //ronaldoPlanet.destructible = NO;
    
    SPIPlanet *benzemaPlanet = [[SPIPlanet alloc] initWithName:@"Karim Benzema"];
    benzemaPlanet.atmosphere = NO;
    //benzemaPlanet.destructible = NO;
    
    SPIPlanet *ramosPlanet = [[SPIPlanet alloc] initWithName:@"Sergio Ramos"];
    ramosPlanet.atmosphere = NO;
    //ramosPlanet.destructible = NO;
    
    SPIStar *zizuStar = [[SPIStar alloc] initWithName:@"Zinedine Zidan"];
    zizuStar.weight = 100000000000;
    zizuStar.type = SPIStarTypeGiant;
    //zenStar.destructible = NO;
    
    realStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[balePlanet, navasAsteroidField, ronaldoPlanet, benzemaPlanet, ramosPlanet, zizuStar]];
    
    //*******************************************MOVING********************************************//
    
    alphaStarSystem.nextStarSystems = @[milkyStarSystem, realStarSystem];
    milkyStarSystem.nextStarSystems  = @[realStarSystem];
    realStarSystem.nextStarSystems = @[milkyStarSystem, alphaStarSystem];
    
    //******************************************SPACESHIP******************************************//
    
    SPIPlayerSpaceship *spaceship = [[SPIPlayerSpaceship alloc] initWithName:@"Falcon"];
    [spaceship loadStarSystem:alphaStarSystem];
    
    //******************************************PLAYING********************************************//
    
    BOOL play = YES;
    
    NSMutableArray *gameObjects = [[NSMutableArray alloc] init];
    [gameObjects addObjectsFromArray:alphaStarSystem.spaceObjects];
    [gameObjects addObject:spaceship];
    [gameObjects addObjectsFromArray:milkyStarSystem.spaceObjects];
    [gameObjects addObjectsFromArray:realStarSystem.spaceObjects];

    while (play) {
        SPIPlayerSpaceshipResponse response = [spaceship waitForCommand];
        
        if (response == SPIPlayerSpaceshipResponseExit) {
            play = NO;
            continue;
        }
        
        for (id<SPIGameObject> gameObject in gameObjects) {
            [gameObject nextTurn];
        }
    }
    return 0;
}
