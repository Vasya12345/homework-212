//
//  SpaceObject.h
//  Lection2
//
//  Created by Vladislav Grigoriev on 05/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SPIGameObject.h"

typedef NS_ENUM(NSInteger, SPISpaceObjectType) {
    SPISpaceObjectTypePlanet,
    SPISpaceObjectTypeAsteroidField,
    SPISpaceObjectTypePlayerSpaceship,
    SPISpaceObjectTypeStar,
};

@interface SPISpaceObject : NSObject <SPIGameObject>

@property (nonatomic, assign, readonly) SPISpaceObjectType type;
@property (nonatomic, strong, readonly) NSString *name;

@property (nonatomic, assign, getter=isDestructible) BOOL destructible;

@property (nonatomic, assign) NSInteger turn;

- (instancetype)initWithType:(SPISpaceObjectType)type name:(NSString *)name;

- (NSString *)title;

@end
