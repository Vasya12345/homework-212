//
//  SPIStar.h
//  Lection2
//
//  Created by Admin on 01.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

@interface SPIStar : SPISpaceObject

typedef NS_ENUM(NSUInteger, SPIStarObjectType) {
    SPIStarTypeGiant,
    SPIStarTypeDwarf,
    SPIStarTypeMedium,
    SPIStarTypeUnknow,
    
};
@property (nonatomic, assign) NSInteger weight;
@property (nonatomic, assign) SPIStarObjectType type;
- (instancetype)initWithName:(NSString *)name;

@end


