//
//  SPIStar.m
//  Lection2
//
//  Created by Admin on 01.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SPIStar.h"

@implementation SPIStar

- (instancetype)initWithName:(NSString *)name {
    self = [super initWithType:SPISpaceObjectTypeStar name:name];
    if (self) {
        
    }
    return self;
}




+ (NSString*) convertTypeToString:(SPIStarObjectType)type {
    switch (type) {
        case SPIStarTypeGiant:
            return @"Giant";
            
        case SPIStarTypeDwarf:
            return @"Dwarf";
            
        case SPIStarTypeMedium:
            return @"Medium";
            
        default:
            return @"Unknow";
    }
}

- (NSString *)description {
    return [NSString stringWithFormat:@"\nStar: %@,\nWeight: %ld, \nType: %@",	
            self.name,
            (long)self.weight,
            [SPIStar convertTypeToString: self.type]];
}

@end
