//
//  PlayerSpaceship.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//


#import "SPIPlayerSpaceship.h"

@implementation SPIPlayerSpaceship

- (instancetype)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _name = name;
    }
    return self;
}

- (void)loadStarSystem:(SPIStarSystem *)starSystem {
    _starSystem = starSystem;
    _currentSpaceObject = starSystem.spaceObjects.firstObject;
}


- (SPIPlayerSpaceshipResponse)waitForCommand {
    char command[255];
    
    
    printf("\n%s", [[self availableCommands] cStringUsingEncoding:NSUTF8StringEncoding]);
    printf("\n%s ",[@"Input command:" cStringUsingEncoding:NSUTF8StringEncoding]);
    
    fgets(command, 255, stdin);
    int commandNumber = atoi(command);
    self.nextCommand = commandNumber;
    
    return self.nextCommand == SPIPlayerSpaceshipCommandExit ? SPIPlayerSpaceshipResponseExit : SPIPlayerSpaceshipResponseOK;
}

- (void)nextTurn {
    
    switch (self.nextCommand) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex > 0) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            printf("%s\n", [[self.currentSpaceObject description] cStringUsingEncoding:NSUTF8StringEncoding]);
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextStarSystem:
        {
            printf("Fly to: \n");
            NSInteger numOfSystem = 1;
            for(SPIStarSystem *starSystem in self.starSystem.nextStarSystems)
            {
                printf("%ld. %s \n", numOfSystem, [starSystem.name cStringUsingEncoding:NSUTF8StringEncoding]);
                numOfSystem++;
            }
            
            char action[255];
            fgets(action, 255, stdin);
            int actionNumber = atoi(action);
            
            self.starSystem = [self.starSystem.nextStarSystems objectAtIndex: actionNumber - 1];
            break;
        }
            
        case SPIPlayerSpaceshipCommandDestroyObject:{
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (self.currentSpaceObject.destructible == YES)
            {
                NSLog(@"\n%@ was destroyed\n", self.currentSpaceObject.name);
                
                switch (self.currentSpaceObject.type) {
                    case SPISpaceObjectTypeStar:{ //transform star to planet
                        SPIPlanet *new = [[SPIPlanet alloc] initWithName:self.currentSpaceObject.name];
                        new.atmosphere = NO;
                        new.peoplesCount = 0;
                        new.destructible = YES;
                        
                        [self.starSystem.spaceObjects replaceObjectAtIndex:currentSpaceObjectIndex withObject:(SPIPlanet*) new];
                        
                        break;
                    }
                    case SPISpaceObjectTypePlanet:{ //transform palnet to asteroid field
                        SPIAsteroidField *new = [[SPIAsteroidField alloc] initWithName:self.currentSpaceObject.name];
                        new.density = ((int)(arc4random() % 100)) - 50;
                        new.destructible = YES;
                        
                        [self.starSystem.spaceObjects replaceObjectAtIndex:currentSpaceObjectIndex withObject:(SPIAsteroidField*) new];
                        
                        break;
                    }
                    case SPISpaceObjectTypeAsteroidField:{ //vanished of asteroid field
                        [self.starSystem.spaceObjects removeObjectAtIndex:currentSpaceObjectIndex];
                        
                        break;
                    }
                        
                    default:
                        break;
                }
                
                if ([self.starSystem.spaceObjects count] < currentSpaceObjectIndex + 1 && [self.starSystem.spaceObjects count] != 0) self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1];
                else if ([self.starSystem.spaceObjects count] != 0) self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex];
                else self.currentSpaceObject = nil;
                
            }
            else{
                NSLog(@"\nYou can't destroy it!");
            }
            break;
        }
            
        default:
            break;
    }
    
    self.nextCommand = 0;
}

- (NSString *)availableCommands {
    if (self.starSystem) {
        NSMutableArray *mutableDescriptions = [[NSMutableArray alloc] init];
        if(self.currentSpaceObject != nil){
            for (NSInteger command = SPIPlayerSpaceshipCommandFlyToPreviousObject; command <= SPIPlayerSpaceshipCommandDestroyObject; command++) {
                NSString *description = [self descriptionForCommand:command];
                if (description) {
                    [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)command, description]];
                }
            }
        }
        else{
            NSLog(@"\nStar system is empty!\n");
        }
        NSString* description = [self descriptionForCommand:SPIPlayerSpaceshipCommandFlyToNextStarSystem];
        if (description) {
            [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)SPIPlayerSpaceshipCommandFlyToNextStarSystem, description]];
        }
        
        description = [self descriptionForCommand:SPIPlayerSpaceshipCommandExit];
        if (description) {
            [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)SPIPlayerSpaceshipCommandExit, description]];
        }
        
        
        return [mutableDescriptions componentsJoinedByString:@"\n"];
    }
    return nil;
}

- (NSString *)descriptionForCommand:(SPIPlayerSpaceshipCommand)command {
    NSString *commandDescription = nil;
    switch (command) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex > 0) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1] title]];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            commandDescription = [NSString stringWithFormat:@"Explore %@", [self.currentSpaceObject title]];
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1] title]];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextStarSystem:
        {
            commandDescription = [NSString stringWithFormat:@"Fly to next star system"];
            break;
        }
            
        case SPIPlayerSpaceshipCommandDestroyObject:
        {
            commandDescription = [NSString stringWithFormat:@"Destroy"];
            break;
        }
            
        case  SPIPlayerSpaceshipCommandExit: {
            commandDescription = [NSString stringWithFormat:@""];
            break;
        }
            
            
        default:
            break;
    }
    return commandDescription;
}


@end
